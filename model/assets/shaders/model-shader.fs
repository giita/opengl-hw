#version 330 core
out vec4 out_color;

in vec3 world_norm_raw;
in vec3 world_position;

uniform vec3 camera_position;
uniform samplerCube cubemap;

void main()
{
    vec3 world_norm = normalize(world_norm_raw);
    vec3 view = normalize(world_position - camera_position);
    vec3 reflected = reflect(view, world_norm);
    vec3 reflection = texture(cubemap, reflected).rgb;

    float eta1 = 1.00;
    float eta2 = 1.77; // sapphire
    float ratio = eta1 / eta2;
    vec3 refracted = refract(view, world_norm, ratio);
    vec3 refraction = texture(cubemap, refracted).rgb;

    float cos_theta_i = abs(dot(view, world_norm));
    float cos_theta_t = abs(dot(world_norm, refracted));

    float r_n = (eta1 * cos_theta_i - eta2 * cos_theta_t) / (eta1 * cos_theta_i + eta2 * cos_theta_t);
    r_n *= r_n;
    float r_t = (eta2 * cos_theta_i - eta1 * cos_theta_t) / (eta2 * cos_theta_i + eta1 * cos_theta_t);
    r_t *= r_t;
    float r = (r_n + r_t) / 2;

    // Assume eta1 < eta2 so no total internal reflection
    vec3 fresnel = r * reflection + (1 - r) * refraction;

    vec3 color = vec3(0.1, 0.1, 0.5);
    float color_part = 0.3;

    out_color = vec4((1 - color_part) * fresnel + color_part * color, 1.0);
}
