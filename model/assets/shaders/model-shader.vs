#version 330 core

layout (location = 0) in vec3 in_pos;
layout (location = 1) in vec3 in_normal;

out vec3 world_norm_raw;
out vec3 world_position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    world_norm_raw = mat3(transpose(inverse(model))) * in_normal;
    world_position = vec3(model * vec4(in_pos, 1.0));
    gl_Position = projection * view * vec4(world_position, 1.0);
}
