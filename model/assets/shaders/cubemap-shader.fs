#version 330 core
 
out vec4 out_color;
in vec3 tex_coords;

uniform samplerCube cubemap;

void main()
{
    out_color = texture(cubemap, tex_coords);
}
