Display a model and a cubemap using OpenGL.

Build using `build.sh`. Run using `model` while in the `build/bin` directory.

Move with mouse. Zoom with mouse wheel. Press Q to exit.

### Demo

![](demo.png)
