#!/bin/bash

set -e
set -x

conan install . -c tools.system.package_manager:mode=install -c tools.system.package_manager:sudo=True

cmake --preset conan-release
cmake --build --preset conan-release
