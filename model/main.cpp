#include <GL/glew.h>
#include <GL/gl.h>

#include <chrono>
#include <cmath>
#include <iostream>
#include <vector>

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "opengl_shader.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

constexpr int MIN_SCROLL = 0;
constexpr int MAX_SCROLL = 50;

constexpr int SHIFT_Y_ABS_CAP = 300;
constexpr int SHIFT_X_WRAP = 4 * SHIFT_Y_ABS_CAP;

int display_w, display_h;
int current_scroll = 5;
int mouse_x, mouse_y;
int shift_start_x, shift_start_y;
float shift_x = 0, shift_y = 0;
bool drag = false;

void glfw_error_callback(int error, const char* description)
{
    std::cerr << "GLFW Error " << error << ": " << description << std::endl;
}

float get_zoom()
{
    return 1 + 3.0 * current_scroll / MAX_SCROLL;
}

void glfw_scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    (void)window;
    (void)xoffset;
    current_scroll += yoffset;
    if (current_scroll > MAX_SCROLL) {
        current_scroll = MAX_SCROLL;
    } else if (current_scroll < MIN_SCROLL) {
        current_scroll = MIN_SCROLL;
    }
}

int shift_x_cap(int shift)
{
    shift %= SHIFT_X_WRAP;
    if (shift < 0) {
        shift += SHIFT_X_WRAP;
    }
    return shift;
}

int shift_y_cap(int shift)
{
    if (shift > SHIFT_Y_ABS_CAP) {
        shift = SHIFT_Y_ABS_CAP;
    } else if (shift < -SHIFT_Y_ABS_CAP) {
        shift = -SHIFT_Y_ABS_CAP;
    }
    return shift;
}

void glfw_cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
    (void)window;
    mouse_x = xpos;
    mouse_y = ypos;
    if (drag) {
        shift_x += mouse_x - shift_start_x;
        shift_y += mouse_y - shift_start_y;
        shift_x = shift_x_cap(shift_x);
        shift_y = shift_y_cap(shift_y);
        shift_start_x = mouse_x;
        shift_start_y = mouse_y;
    }
}

void glfw_mouse_button_callback(GLFWwindow* window,
                                int button,
                                int action,
                                int mods)
{
    (void)window;
    (void)mods;
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        drag = action == GLFW_PRESS;
        if (drag) {
            shift_start_x = mouse_x;
            shift_start_y = mouse_y;
        }
    }
}

void glfw_key_callback(GLFWwindow* window,
                       int key,
                       int scancode,
                       int action,
                       int mods)
{
    (void)scancode;
    (void)mods;
    if (key == GLFW_KEY_Q && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
}

void glfw_framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    (void)window;
    display_h = height;
    display_w = width;
    glViewport(0, 0, width, height);
}


void init_obj(const std::string &&path, GLuint &vao, GLuint &vbo, int &data_elements) {
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string err;

    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, path.c_str());
    if (!err.empty()) {
        std::cerr << err << std::endl;
    }
    if (!ret) {
        exit(1);
    }

    std::vector<float> data;
    for (size_t s = 0; s < shapes.size(); s++) {
        size_t index_offset = 0;
        for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
            int fv = shapes[s].mesh.num_face_vertices[f];
            if (fv != 3) {
                std::cerr << "Some face is not a triangle" << std::endl;
                exit(1);
            }
            for (int v = 0; v < fv; v++) {
                tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
                for (int i = 0; i < 3; ++i) {
                    data.push_back(attrib.vertices[3*idx.vertex_index+i]);
                }
                for (int i = 0; i < 3; ++i) {
                    data.push_back(attrib.normals[3*idx.normal_index+i]);
                }
            }
            index_offset += fv;
        }
    }
    data_elements = data.size() / 6;

    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);

    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), data.data(), GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3*sizeof(float)));
}

void init_cubemap(GLuint &vao, GLuint &vbo, GLuint &ebo) {
    static GLfloat vertices[] = {
        1.0, 1.0, 1.0,
        1.0, 1.0, -1.0,
        1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,
        -1.0, 1.0, 1.0,
        -1.0, 1.0, -1.0,
        -1.0, -1.0, 1.0,
        -1.0, -1.0, -1.0,
    };
    static GLuint indices[] = {
        5, 7, 3,
        3, 1, 5,
        
        6, 7, 5,
        5, 4, 6,
        
        3, 2, 0,
        0, 1, 3,
        
        6, 4, 0,
        0, 2, 6,
        
        5, 1, 0,
        0, 4, 5,
        
        7, 6, 3,
        3, 6, 2,
    };
    
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);
    
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices,
                 GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
}    

GLuint create_cubemap_texture(const std::vector<std::string> &cubemap_faces)
{
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    
    int width, height, num_channels;
    unsigned char *data;  
    for(unsigned int i = 0; i < cubemap_faces.size(); i++)
    {
        data = stbi_load(cubemap_faces[i].c_str(), &width, &height, &num_channels, 0);
        glTexImage2D(
            GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 
            0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data
        );
        stbi_image_free(data);
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    
    return texture;
}

void gl_init()
{
    glEnable(GL_MULTISAMPLE);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    glEnable(GL_DEPTH_TEST);
}

int main(int, char**)
{
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, 4);

    GLFWwindow* window = glfwCreateWindow(1280, 720, "Model", NULL, NULL);
    if (window == NULL)
        return 1;

    glfwSetScrollCallback(window, glfw_scroll_callback);
    glfwSetCursorPosCallback(window, glfw_cursor_position_callback);
    glfwSetMouseButtonCallback(window, glfw_mouse_button_callback);
    glfwSetKeyCallback(window, glfw_key_callback);
    glfwSetFramebufferSizeCallback(window, glfw_framebuffer_size_callback);

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    if (glewInit() != GLEW_OK) {
        std::cerr << "Failed to initialize OpenGL loader\n";
        return 1;
    }

    gl_init();

    GLuint model_vao, model_vbo;
    int model_elements;
    init_obj("models/knight_statue_fixed.obj", model_vao, model_vbo, model_elements);

    shader_t model_shader("shaders/model-shader.vs",
                          "shaders/model-shader.fs");
    
    GLuint cubemap_vao, cubemap_vbo, cubemap_ebo;
    init_cubemap(cubemap_vao, cubemap_vbo, cubemap_ebo);

    std::vector<std::string> cubemap_faces {
        "cubemaps/Brudslojan/posx.jpg",
        "cubemaps/Brudslojan/negx.jpg",
        "cubemaps/Brudslojan/posy.jpg",
        "cubemaps/Brudslojan/negy.jpg",
        "cubemaps/Brudslojan/posz.jpg",
        "cubemaps/Brudslojan/negz.jpg",
    };
    GLuint cubemap_texture = create_cubemap_texture(cubemap_faces);

    shader_t cubemap_shader("shaders/cubemap-shader.vs",
                            "shaders/cubemap-shader.fs");

    glm::vec3 camera_position;
    glm::mat4 model_matrix, view_matrix, projection_matrix;
    model_matrix = glm::translate(glm::vec3(0.0f, -1.8f, -2.5f));

    glfwGetFramebufferSize(window, &display_w, &display_h);

    while (!glfwWindowShouldClose(window)) {
        glDepthFunc(GL_LESS);
        glDepthMask(GL_TRUE);
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        projection_matrix = glm::perspective(glm::radians(70.0f), (float)display_w / (float)display_h, 0.1f, 100.0f);
        
        camera_position = glm::vec3(0.0f, 0.0f, 5.0f);
        float vert = (shift_y / SHIFT_Y_ABS_CAP) * (glm::pi<float>() / 2) * 0.9;
        float hori = (shift_x / SHIFT_X_WRAP) * glm::pi<float>() * 2;
        camera_position = glm::rotateX(camera_position, -vert);
        camera_position = glm::rotateY(camera_position, -hori);
        camera_position = camera_position / get_zoom();

        view_matrix = glm::lookAt(
            camera_position,
            glm::vec3(0.0f, 0.0f, 0.0f),
            glm::vec3(0.0f, 1.0f, 0.0f)
        );
  
        glm::mat4 view_rotate_only = projection_matrix * glm::mat4(glm::mat3(view_matrix));

        // depth pre-pass
        glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
        model_shader.use();
        model_shader.set_uniform("model", &model_matrix[0][0]);
        model_shader.set_uniform("view", &view_matrix[0][0]);
        model_shader.set_uniform("projection", &projection_matrix[0][0]);
        model_shader.set_uniform("camera_position", camera_position.x, camera_position.y, camera_position.z);
        glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap_texture);
        glBindVertexArray(model_vao);
        glDrawArrays(GL_TRIANGLES, 0, model_elements);

        // actual rendering
        glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
        glDepthMask(GL_FALSE);
        glDepthFunc(GL_EQUAL);
        glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap_texture);
        glBindVertexArray(model_vao);
        glDrawArrays(GL_TRIANGLES, 0, model_elements);

        // now we can render cubemap
        // note that in cubemap-shader.vs we set z to 1.0
        glDepthFunc(GL_LEQUAL);
        cubemap_shader.use();
        cubemap_shader.set_uniform("transform", &view_rotate_only[0][0]);
        glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap_texture);
        glBindVertexArray(cubemap_vao);
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
