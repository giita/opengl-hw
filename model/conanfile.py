from conan import ConanFile
from conan.tools.cmake import cmake_layout

class Pkg(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeDeps", "CMakeToolchain"

    def requirements(self):
        self.requires("glfw/3.3.2")
        self.requires("glew/2.1.0")
        self.requires("glm/0.9.9.8")
        self.requires("tinyobjloader/1.0.6")

    def layout(self):
        cmake_layout(self)

