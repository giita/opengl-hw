Fractal using ImGui and OpenGL.

Build using `build.sh`. Run using `fractal` while in the `build` directory.

Move with mouse. Zoom with mouse wheel. Press Q to exit.

### Demo

![](demo.png)
