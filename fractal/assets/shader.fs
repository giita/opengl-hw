#version 330 core
 
out vec4 color;
uniform int u_iterations;
uniform float u_radius;
uniform float u_julia_cr;
uniform float u_julia_ci;
uniform float u_zoom;
uniform int u_color_wrap;
uniform vec2 u_center;
uniform sampler1D u_tex;
 
int get_iterations()
{
    float a = (gl_FragCoord.x - u_center[0]) / u_zoom;
    float b = (gl_FragCoord.y - u_center[1]) / u_zoom;
 
    int i;
    for (i = 0; i < u_iterations; ++i)
    {
        float x = a, y = b;
        
        a = x * x - y * y + u_julia_cr;
        b = 2 * x * y + u_julia_ci;
        
        if (a * a + b * b > u_radius * u_radius)
            break;
    }
    return i;
}
 
vec4 color_for(int iterations)
{
    if (iterations == u_iterations) {
        return vec4(0.0f, 0.0f, 0.0f, 1.0f);
    }
    float iters_normal = float(iterations) / u_color_wrap;
    return texture(u_tex, iters_normal);
}
 
void main()
{
    color = color_for(get_iterations());
}
