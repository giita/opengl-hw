#include <GL/glew.h>
#include <GL/gl.h>

#include <chrono>
#include <cmath>
#include <iostream>
#include <vector>

// Imgui + bindings
#include "imgui.h"
#include "bindings/imgui_impl_glfw.h"
#include "bindings/imgui_impl_opengl3.h"

// Include glfw3.h after our OpenGL definitions
#include <GLFW/glfw3.h>

// Math constant and routines for OpenGL interop
#include <glm/gtc/constants.hpp>

#include "opengl_shader.h"

int display_w, display_h;
int current_scroll = -20;
int mouse_x, mouse_y;
int shift_start_x, shift_start_y;
float shift_x = 0, shift_y = 0;
bool drag = false;

void glfw_error_callback(int error, const char* description)
{
    std::cerr << "GLFW Error " << error << ": " << description << '\n';
}

#define MIN_SCROLL -50
#define MAX_SCROLL 350

float get_zoom()
{
    return std::pow(2, current_scroll / 15.0) * std::min(display_h, display_w);
}

void glfw_scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    float old_zoom = get_zoom();
    current_scroll += yoffset;
    current_scroll = std::max(MIN_SCROLL, current_scroll);
    current_scroll = std::min(MAX_SCROLL, current_scroll);
    float zoom = get_zoom() / old_zoom;

    // (mouse_x - shift_x_old) / old_zoom == (mouse_x - shift_x_new) / new_zoom
    // shift_x_new / new_zoom = -(mouse_x - shift_x_old) / old_zoom + mouse_x /
    // new_zoom shift_x_new = -(mouse_x - shift_x_old) * zoom + mouse_x
    shift_x = -(mouse_x - shift_x) * zoom + mouse_x;
    // (display_h - mouse_y + shift_y_old) / old_zoom == (display_h - mouse_y +
    // shift_y_new) / new_zoom shift_y_new / new_zoom = (display_h - mouse_y +
    // shift_y) / old_zoom - (display_h - mouse_y) / new_zoom shift_y_new =
    // (display_h - mouse_y + shift_y) * zoom - mouse_y
    shift_y = (display_h - mouse_y + shift_y) * zoom - (display_h - mouse_y);
}

void glfw_cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
    mouse_x = xpos;
    mouse_y = ypos;
    if (drag) {
        shift_x += mouse_x - shift_start_x;
        shift_y += mouse_y - shift_start_y;
        shift_start_x = mouse_x;
        shift_start_y = mouse_y;
    }
}

void glfw_mouse_button_callback(GLFWwindow* window,
                                int button,
                                int action,
                                int mods)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        drag = action == GLFW_PRESS;
        if (drag) {
            shift_start_x = mouse_x;
            shift_start_y = mouse_y;
        }
    }
}

void glfw_key_callback(GLFWwindow* window,
                       int key,
                       int scancode,
                       int action,
                       int mods)
{
    if (key == GLFW_KEY_Q && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
}

void create_square(GLuint& vbo, GLuint& vao, GLuint& ebo)
{
    GLfloat vertices[] = {
        1.0f,  1.0f,   //
        -1.0f, 1.0f,   //
        -1.0f, -1.0f,  //
        1.0f,  -1.0f,  //
    };
    GLuint indices[] = {
        0, 1, 2,  //
        0, 2, 3,  //
    };
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);

    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices,
                 GL_STATIC_DRAW);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float),
                          (void*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void create_texture(GLuint& texture)
{
    glGenTextures(1, &texture);
    glActiveTexture(GL_TEXTURE0 + 0);
    glBindTexture(GL_TEXTURE_1D, texture);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void apply_texture_colors(GLuint& texture, std::vector<GLfloat>& colors)
{
    glBindTexture(GL_TEXTURE_1D, texture);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, colors.size() / 3, 0, GL_RGB,
                 GL_FLOAT, colors.data());
    glGenerateMipmap(GL_TEXTURE_1D);
}

int main(int, char**)
{
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;

    // GL 3.3 + GLSL 330
    const char* glsl_version = "#version 330";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, 4);
    // glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // 3.0+ only

    // Create window with graphics context
    GLFWwindow* window = glfwCreateWindow(1280, 720, "Fractal", NULL, NULL);
    if (window == NULL)
        return 1;

    glfwSetScrollCallback(window, glfw_scroll_callback);
    glfwSetCursorPosCallback(window, glfw_cursor_position_callback);
    glfwSetMouseButtonCallback(window, glfw_mouse_button_callback);
    glfwSetKeyCallback(window, glfw_key_callback);

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);  // Enable vsync

    // Initialize GLEW, i.e. fill all possible function pointers for current
    // OpenGL context
    if (glewInit() != GLEW_OK) {
        std::cerr << "Failed to initialize OpenGL loader\n";
        return 1;
    }

    glEnable(GL_MULTISAMPLE);

    // create our geometries
    GLuint vbo, vao, ebo, texture;
    create_square(vbo, vao, ebo);

    create_texture(texture);

    static std::vector<GLfloat> colors{
        0.0, 0.0, 0.0,  // Black
        1.0, 0.0, 0.0,  // Red
        1.0, 1.0, 0.0,  // Yellow
        0.0, 0.0, 1.0,  // Blue
    };
    apply_texture_colors(texture, colors);

    // init shader
    shader_t shader("shader.vs", "shader.fs");

    shader.set_uniform("u_tex", 0);

    // Setup GUI context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);
    ImGui::StyleColorsDark();

    glfwPollEvents();
    glfwGetFramebufferSize(window, &display_w, &display_h);
    shift_x = display_w / 2;
    shift_y = -display_h / 2;

    while (!glfwWindowShouldClose(window)) {
        glfwPollEvents();

        // Get windows size
        glfwGetFramebufferSize(window, &display_w, &display_h);

        // Set viewport to fill the whole window area
        glViewport(0, 0, display_w, display_h);

        // Fill background with solid color
        glClearColor(0.30f, 0.55f, 0.60f, 1.00f);
        glClear(GL_COLOR_BUFFER_BIT);

        // Gui start new frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        // GUI
        ImGui::Begin("Settings", nullptr, ImGuiWindowFlags_AlwaysAutoResize);

        ImGui::Text("Julia parameters");
        static float c = 0.772;
        ImGui::SliderFloat("c", &c, 0, 1);
        static float a = 4.580;
        ImGui::SliderFloat("a", &a, 0, 2 * glm::pi<float>());

        ImGui::Text("Quality");
        static int iterations = 1300;
        ImGui::SliderInt("iterations", &iterations, 100, 4000);
        static float radius = 10.0;
        ImGui::SliderFloat("radius", &radius, 1.0, 30.0);

        ImGui::Text("Display");
        static int iterations_color_wrap = 200;
        ImGui::SliderInt("color wrap", &iterations_color_wrap, 100, 1000);

        ImGui::Text("Colors");
        bool colors_changed = false;
        for (int i = 0; i < colors.size() / 3; ++i) {
            colors_changed |= ImGui::ColorEdit3(
                (std::string("color ") + std::string(1, '1' + i)).c_str(),
                colors.data() + i * 3);
        }

        ImGui::End();

        // Pass the parameters to shader as uniforms
        shader.set_uniform("u_julia_cr", c * (float)std::sin(a));
        shader.set_uniform("u_julia_ci", c * (float)std::cos(a));

        shader.set_uniform("u_iterations", iterations);
        shader.set_uniform("u_radius", radius);
        shader.set_uniform("u_color_wrap", iterations_color_wrap);

        shader.set_uniform("u_zoom", get_zoom());
        shader.set_uniform("u_center", shift_x, -shift_y);
        if (colors_changed) {
            apply_texture_colors(texture, colors);
        }
        // Bind shader
        shader.use();

        // Bind vertex array = buffers + indices
        glBindVertexArray(vao);
        // Execute draw call
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);

        // Generate gui render commands
        ImGui::Render();

        // Execute gui render commands using OpenGL backend
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        // Swap the backbuffer with the frontbuffer that is used for screen
        // display
        glfwSwapBuffers(window);
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
    glDeleteBuffers(1, &ebo);

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
