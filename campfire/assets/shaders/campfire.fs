#version 330 core

uniform sampler2D tex;
uniform float time;
uniform float life;
uniform bool output_alpha;
uniform float onset;

in float time_born;
in vec2 tex_coord;
out vec4 out_color;

void main()
{
    vec4 c = texture(tex, tex_coord);
    float mult;
    float life_part = min(life, time - time_born) / life;
    if (life_part < onset) {
        mult = life_part / onset;
    } else {
        mult = (1 - life_part) / (1 - onset);
    }
    out_color = vec4(output_alpha ? vec3(0.) : c.rgb, c.a * mult);
}
