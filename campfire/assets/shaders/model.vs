#version 330 core

layout (location = 0) in vec3 in_pos;
layout (location = 1) in vec3 in_normal;
layout (location = 2) in vec2 in_tex_coords;

out vec2 tex_coords;
out vec3 normal_l;
out vec3 frag_pos;
out vec4 frag_pos_light_space;

uniform mat4 model;
uniform mat3 normal_model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 proj_space;
uniform mat4 light_space;

void main()
{
    vec4 world_position = model * vec4(in_pos, 1.0);

    tex_coords = in_tex_coords;
    normal_l = normalize(normal_model * in_normal);
    frag_pos = vec3(world_position);
    frag_pos_light_space = light_space * world_position;
    gl_Position = projection * view * world_position;
}
