#version 330 core

layout (location = 0) in vec3 in_pos;
layout (location = 1) in float in_alive;
layout (location = 2) in float in_time_born;

uniform mat4 model;
uniform mat4 view;
out float alive_gs;
out float time_born_gs;

void main()
{
    vec4 world_position = model * vec4(in_pos, 1.0);
    alive_gs = in_alive;
    time_born_gs = in_time_born;
    gl_Position = view * world_position;
}
