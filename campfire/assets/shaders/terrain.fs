#version 330 core

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

uniform sampler2D tex;
uniform sampler2D shadow_map_depth;
uniform sampler2D shadow_map_translucency;
uniform Material material;
uniform vec3 light_color;
uniform vec3 light_dir;
uniform vec3 camera_pos;
uniform float detail_scale;

in vec2 tex_coords;
in vec3 normal_l;
in vec3 frag_pos;
in vec4 frag_pos_light_space;

out vec3 out_color;

vec3 normal;

vec3 get_tex(vec2 pos)
{
    return texture(tex, pos).rgb;
}

float get_shadow()
{
    vec3 proj_coords = frag_pos_light_space.xyz / frag_pos_light_space.w;
    proj_coords = proj_coords * 0.5 + 0.5;
    float closest_depth = texture(shadow_map_depth, proj_coords.xy).r;
    float current_depth = proj_coords.z;
    if (current_depth > 1.) {
        return 0.;
    }
    float bias = 0.005;
    if (current_depth - bias  < closest_depth) {
        return 1. - texture(shadow_map_translucency, proj_coords.xy).r;
    }
    return 1.;
}

vec4 get_detail()
{
    float d = distance(frag_pos, camera_pos);
    return vec4(get_tex(tex_coords * 10), 0.5 * (1 - smoothstep(40, 80, d)));
}

void main()
{
    normal = normalize(normal_l);

    // ambient
    vec3 ambient = light_color * material.ambient * 0.1;

    // diffuse
    float diff = max(dot(normal, -light_dir), 0.0);
    vec3 diffuse = light_color * (diff * material.diffuse);

    // specular
    vec3 view_dir = normalize(camera_pos - frag_pos);
    vec3 reflect_dir = reflect(light_dir, normal);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0), material.shininess);
    vec3 specular = light_color * (spec * material.specular);

    vec4 d = get_detail();
    vec3 c = mix(get_tex(tex_coords), d.rgb, d.a);
    out_color = (ambient + (1.0 - get_shadow()) * (diffuse + specular)) * c;
}
