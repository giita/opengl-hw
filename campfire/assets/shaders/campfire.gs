#version 330 core
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;
out vec2 tex_coord;
out float time_born;
in float alive_gs[];
in float time_born_gs[];
uniform mat4 projection;
uniform float scale;

void main() {
    if (alive_gs[0] < 0.5) {
        return;
    }
    time_born = time_born_gs[0];
    vec4 position = gl_in[0].gl_Position;
    for (int i = 0; i < 4; ++i) {
        gl_Position = projection * (position + scale * vec4((i % 2) == 0 ? -1 : 1, i < 2 ? -1 : 1, 0.0, 0.0));
        tex_coord = vec2((i % 2) == 0 ? 0 : 1, i < 2 ? 0 : 1);
        EmitVertex();
    }
    EndPrimitive();
}
