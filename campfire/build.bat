@ECHO ON

conan install . --build=missing
cmake --preset conan-default
cmake --build --preset conan-release
