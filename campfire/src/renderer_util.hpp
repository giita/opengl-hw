#pragma once

#include <cstdlib>
#include <iostream>

#include <GL/glew.h>
#include <GL/gl.h>

#include <GLFW/glfw3.h>

#include "glfw_callbacks.hpp"

inline void glfw_init()
{
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit()) {
        exit(1);
    }
}

inline void init_gl()
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
}

inline void glew_init()
{
    if (glewInit() != GLEW_OK) {
        std::cerr << "Failed to initialize OpenGL loader\n";
        exit(1);
    }
}
