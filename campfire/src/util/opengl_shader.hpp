#pragma once

#include <string>
#include <vector>

#include <GL/glew.h>
#include <GL/gl.h>

#include <glm/glm.hpp>

class shader_t
{
   public:
    shader_t(const std::string& vertex_code_fname,
             const std::string& fragment_code_fname,
             const std::string& geometry_code_fname = "");
    ~shader_t();

    void use();
    void set_uniform(const std::string& name, const int val);
    void set_uniform(const std::string& name, const bool val);
    void set_uniform(const std::string& name, const float val);
    void set_uniform(const std::string& name, const glm::vec2& val);
    void set_uniform(const std::string& name, const glm::vec3& val);
    void set_uniform(const std::string& name, const glm::vec4& val);
    void set_uniform(const std::string& name, const glm::mat3& val);
    void set_uniform(const std::string& name, const glm::mat4& val);
    void set_uniform_f3(const std::string& name, const float* const val);

   private:
    void check_compile_error();
    void check_linking_error();
    void compile(const std::string& vertex_code,
                 const std::string& fragment_code,
                 const std::string& geometry_code);
    void link();

    GLuint vertex_id_, fragment_id_, geometry_id_, program_id_;
};
