#pragma once

#include <GLFW/glfw3.h>

void glfw_error_callback(int error, const char* description);

void glfw_scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

void glfw_cursor_position_callback(GLFWwindow* window,
                                   double xpos,
                                   double ypos);

void glfw_mouse_button_callback(GLFWwindow* window,
                                int button,
                                int action,
                                int mods);

void glfw_key_callback(GLFWwindow* window,
                       int key,
                       int scancode,
                       int action,
                       int mods);

void glfw_framebuffer_size_callback(GLFWwindow* window, int width, int height);
