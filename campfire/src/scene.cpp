#include <memory>

#include "objects/fps_counter.hpp"
#include "objects/sun.hpp"
#include "objects/shadow_map.hpp"
#include "objects/campfire.hpp"
#include "objects/skybox.hpp"
#include "objects/terrain.hpp"

#include "renderer.hpp"
#include "util/INIReader.h"

void renderer::init_scene()
{
    INIReader reader("assets/config.ini");

    auto fps = std::make_shared<class fps_counter>(1, 1, 20);
    objects.push_back(fps);

    auto sun = std::make_shared<class sun>();
    sun->dir = glm::vec3(reader.GetFloat("sun_dir", "x", 0.),
                         reader.GetFloat("sun_dir", "y", 0.),
                         reader.GetFloat("sun_dir", "z", 0.));
    sun->color = glm::vec3(reader.GetFloat("sun_color", "r", 0.),
                           reader.GetFloat("sun_color", "g", 0.),
                           reader.GetFloat("sun_color", "b", 0.));
    objects.push_back(sun);

    auto shadow = std::make_shared<shadow_map>("assets/shaders/shadow.vs",
                                               "assets/shaders/shadow.fs");
    objects.push_back(shadow);

    auto skybox = std::make_shared<class skybox>(
        "assets/shaders/skybox.vs", "assets/shaders/skybox.fs",
        std::array<std::string, 6>{
            reader.Get("skybox", "right", ""),
            reader.Get("skybox", "left", ""),
            reader.Get("skybox", "up", ""),
            reader.Get("skybox", "down", ""),
            reader.Get("skybox", "front", ""),
            reader.Get("skybox", "back", ""),
        });
    skybox->reflected = true;
    objects.push_back(skybox);

    auto ground = std::make_shared<terrain>(
        "assets/shaders/terrain.vs", "assets/shaders/terrain.fs",
        reader.Get("ground", "heightmap", ""),
        reader.Get("ground", "texture", ""));
    ground->position = glm::vec3(reader.GetFloat("ground_pos", "x", 0.),
                                 reader.GetFloat("ground_pos", "y", 0.),
                                 reader.GetFloat("ground_pos", "z", 0.));
    ground->h = reader.GetFloat("ground", "h", 3.);
    ground->horiz = reader.GetFloat("ground", "horiz", 100.);
    ground->texture_size = reader.GetFloat("ground", "texture_size", 20.);
    ground->casts_solid_shadow = true;
    ground->reflected = true;
    objects.push_back(ground);

    auto campfire = std::make_shared<class campfire>(
        "assets/shaders/campfire.vs", "assets/shaders/campfire.fs",
        "assets/shaders/campfire.gs", reader.Get("campfire", "fire_tex", ""),
        reader.Get("campfire", "smoke_tex", ""));
    campfire->scale = reader.GetFloat("campfire", "scale", 1.);
    campfire->rotate = reader.GetFloat("campfire", "rotate", 0.);
    objects.push_back(campfire);

    stbi_set_flip_vertically_on_load(true);
    for (auto& d : objects) {
        d->load_resources();
        d->init_gl();
    }

    campfire->position =
        ground->to_surface(
            glm::vec3(reader.GetFloat("campfire_pos", "x", 0.), 0.,
                      reader.GetFloat("campfire_pos", "z", 0.))) +
        glm::vec3(0., reader.GetFloat("campfire", "elevation", 0.), 0.);

    context.camera_position = glm::vec3(reader.GetFloat("camera_pos", "x", 0.),
                                        reader.GetFloat("camera_pos", "y", 0.),
                                        reader.GetFloat("camera_pos", "z", 0.));
    cursor_x = reader.GetFloat("cursor", "x", 0.);
    cursor_y = reader.GetFloat("cursor", "y", 0.);
}

void renderer::draw_scene()
{
    glViewport(0, 0, display_w, display_h);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    for (auto& d : objects) {
        d->draw(context);
    }
}
