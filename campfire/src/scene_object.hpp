#pragma once

#include <vector>
#include <memory>

#include <glm/glm.hpp>
#include <imgui.h>

constexpr int LOCAL_TEXTURE0 = 2;
constexpr int SHADOW_MAP_DEPTH_TEXTURE = 0;
constexpr int SHADOW_MAP_TRANSLUCENCY_TEXTURE = 1;

class scene_object;

class draw_context
{
   public:
    draw_context() {}
    ImGuiIO* imgui_io;
    long long frame_number;
    float time;
    glm::mat4 view;
    glm::mat4 projection;
    glm::vec3 camera_position;
    glm::vec3 camera_direction;
    bool drawing_geometry_only = false;
    bool drawing_particles_shadow = false;
    glm::mat4 light_view = glm::mat4(1.);
    glm::mat4 light_projection = glm::mat4(1.);
    glm::vec3 light_dir;
    glm::vec3 light_color;
    std::vector<std::shared_ptr<scene_object>>* objects;
};

class scene_object
{
   public:
    void (*animate)(scene_object*, float, void* data) =
        [](scene_object*, float, void*) {};
    void* animation_data;
    bool casts_solid_shadow = false;
    bool particles = false;
    bool reflected = false;
    bool global_config = false;
    bool drawable = true;
    virtual void load_resources() {}
    virtual void init_gl() {}
    virtual glm::mat4 get_model_matrix() { return glm::mat4(1.); }
    virtual glm::mat3 get_normal_model_matrix()
    {
        return glm::mat3(glm::transpose(glm::inverse(get_model_matrix())));
    }
    virtual void config_context(draw_context& context) {}
    virtual void draw(const draw_context& context) {}
    virtual void update(const draw_context& context) {}
    virtual ~scene_object(){};
};
