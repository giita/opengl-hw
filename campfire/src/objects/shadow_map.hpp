#pragma once

#include "../scene_object.hpp"

#include <memory>
#include <string>

#include <GL/glew.h>
#include <GL/gl.h>

#include "../util/opengl_shader.hpp"

inline constexpr unsigned int SHADOW_WIDTH = 2048, SHADOW_HEIGHT = 2048;

class shadow_map : public scene_object
{
   private:
    std::string fragment_shader;
    std::string vertex_shader;

    GLuint texture_depth, texture_translucency, fbo;

    std::unique_ptr<shader_t> shader;

   public:
    shadow_map(const std::string& vertex_shader,
               const std::string& fragment_shader);

    void load_resources();
    void init_gl();
    void config_context(draw_context& context);
};

inline shadow_map::shadow_map(const std::string& vertex_shader,
                              const std::string& fragment_shader)
{
    this->vertex_shader = vertex_shader;
    this->fragment_shader = fragment_shader;
    global_config = true;
    drawable = false;
}

inline void shadow_map::load_resources()
{
    shader = std::make_unique<shader_t>(vertex_shader, fragment_shader);
}

inline void shadow_map::init_gl()
{
    glGenFramebuffers(1, &fbo);

    glGenTextures(1, &texture_depth);
    glBindTexture(GL_TEXTURE_2D, texture_depth);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH,
                 SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    float borderColor[] = {1.0f, 1.0f, 1.0f, 1.0f};
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

    glGenTextures(1, &texture_translucency);
    glBindTexture(GL_TEXTURE_2D, texture_translucency);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, SHADOW_WIDTH, SHADOW_HEIGHT, 0,
                 GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    float borderColorTranslucency[] = {1.0f, 1.0f, 1.0f, 1.0f};
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
                           texture_depth, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                           texture_translucency, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void shadow_map::config_context(draw_context& context)
{
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
    glClearColor(1., 1., 1., 1.);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    // glCullFace(GL_FRONT);
    draw_context shadow_map_context = context;
    shadow_map_context.drawing_geometry_only = true;
    shadow_map_context.view = context.light_view;
    shadow_map_context.projection = context.light_projection;
    shader->use();
    for (auto& d : *context.objects) {
        if (d->casts_solid_shadow) {
            glm::mat4 model = d->get_model_matrix();
            shader->set_uniform("model", model);
            shader->set_uniform("view", shadow_map_context.view);
            shader->set_uniform("projection", shadow_map_context.projection);
            d->draw(shadow_map_context);
        }
    }
    shadow_map_context.drawing_geometry_only = false;
    shadow_map_context.drawing_particles_shadow = true;
    for (auto& d : *context.objects) {
        if (d->particles) {
            d->draw(shadow_map_context);
        }
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    // glCullFace(GL_BACK);
    glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);

    glActiveTexture(GL_TEXTURE0 + SHADOW_MAP_DEPTH_TEXTURE);
    glBindTexture(GL_TEXTURE_2D, texture_depth);
    glActiveTexture(GL_TEXTURE0 + SHADOW_MAP_TRANSLUCENCY_TEXTURE);
    glBindTexture(GL_TEXTURE_2D, texture_translucency);
}
