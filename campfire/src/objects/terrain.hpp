#pragma once

#include "../scene_object.hpp"

#include <string>
#include <memory>

#include <GL/glew.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "../util/opengl_shader.hpp"

#include "../util/stb_image.h"

class terrain : public scene_object
{
   private:
    std::string fragment_shader;
    std::string vertex_shader;
    std::string height_map;
    std::string tex_file;

    int width, height;
    std::vector<std::vector<float>> elevation;

    GLuint texture;
    GLuint vao, vbo, ebo;

    std::unique_ptr<shader_t> shader;

   public:
    glm::vec3 position;
    float horiz = 1;
    float h = 1;
    float texture_size = 0.2;
    float detail_size = 0.02;

    terrain(const std::string& vertex_shader,
            const std::string& fragment_shader,
            const std::string& height_map,
            const std::string& tex_file);

    void load_resources();
    void init_gl();
    glm::mat4 get_model_matrix();
    void draw(const draw_context& context);

    glm::vec3 to_surface(const glm::vec3& position);
};

inline terrain::terrain(const std::string& vertex_shader,
                        const std::string& fragment_shader,
                        const std::string& height_map,
                        const std::string& tex_file)
{
    this->vertex_shader = vertex_shader;
    this->fragment_shader = fragment_shader;
    this->height_map = height_map;
    this->tex_file = tex_file;
}

inline void terrain::load_resources()
{
    shader = std::make_unique<shader_t>(vertex_shader, fragment_shader);

    int num_channels;
    stbi_us* sdata =
        stbi_load_16(height_map.c_str(), &width, &height, &num_channels, 0);

    elevation =
        std::vector<std::vector<float>>(height, std::vector<float>(width));
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            elevation[i][j] = sdata[i * width + j] / 65536.;
        }
    }

    stbi_image_free(sdata);

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    int tex_width, tex_height, tex_num_channels;

    stbi_set_flip_vertically_on_load(false);
    stbi_uc* data = stbi_load(tex_file.c_str(), &tex_width, &tex_height,
                              &tex_num_channels, 0);
    stbi_set_flip_vertically_on_load(true);

    int mode = tex_num_channels == 3 ? GL_RGB : GL_RGBA;

    glTexImage2D(GL_TEXTURE_2D, 0, mode, tex_width, tex_height, 0, mode,
                 GL_UNSIGNED_BYTE, data);
    stbi_image_free(data);
}

inline void terrain::init_gl()
{
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);
    glBindVertexArray(vao);

    std::vector<float> buffer;

    float texture_scale = texture_size / horiz;

    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            float x = 1. * i / (height - 1) - 0.5;
            float z = 1. * j / (width - 1) - 0.5;
            buffer.push_back(x);
            buffer.push_back(elevation[i][j]);
            buffer.push_back(z);

            glm::vec3 normal;
            if (i == 0 || i == height - 1 || j == 0 || j == width - 1) {
                normal = glm::vec3(0., 1., 0.);
            } else {
                glm::vec3 xd =
                    glm::vec3(2. / (height - 1),
                              elevation[i + 1][j] - elevation[i - 1][j], 0.);
                glm::vec3 zd =
                    glm::vec3(0., elevation[i][j + 1] - elevation[i][j - 1],
                              2. / (width - 1));
                normal = glm::normalize(glm::cross(zd, xd));
            }

            buffer.push_back(normal.x);
            buffer.push_back(normal.y);
            buffer.push_back(normal.z);

            buffer.push_back(x / texture_scale);
            buffer.push_back(z / texture_scale);
        }
    }

    std::vector<GLuint> indices;

    for (int i = 0; i < height - 1; ++i) {
        for (int j = 0; j < width - 1; ++j) {
            int base = i * width + j;
            indices.push_back(base);
            indices.push_back(base + 1);
            indices.push_back(base + width);

            indices.push_back(base + 1);
            indices.push_back(base + width + 1);
            indices.push_back(base + width);
        }
    }

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, buffer.size() * sizeof(float), buffer.data(),
                 GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint),
                 indices.data(), GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
                          (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
                          (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
                          (void*)(6 * sizeof(float)));

    glBindTexture(GL_TEXTURE_2D, texture);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

inline glm::mat4 terrain::get_model_matrix()
{
    return glm::translate(position) * glm::scale(glm::vec3(horiz, h, horiz));
}

inline void terrain::draw(const draw_context& context)
{
    if (!context.drawing_geometry_only) {
        shader->use();
        glm::mat4 model = get_model_matrix();

        shader->set_uniform("model", model);
        shader->set_uniform("view", context.view);
        shader->set_uniform("projection", context.projection);
        shader->set_uniform("normal_model", get_normal_model_matrix());
        shader->set_uniform("light_dir", context.light_dir);
        shader->set_uniform("light_color", context.light_color);
        shader->set_uniform("camera_pos", context.camera_position);
        shader->set_uniform("light_view", context.light_view);
        shader->set_uniform("light_projection", context.light_projection);

        shader->set_uniform("material.ambient", glm::vec3(1.));
        shader->set_uniform("material.diffuse", glm::vec3(1.));
        shader->set_uniform("material.specular", glm::vec3(0.03));
        shader->set_uniform("material.shininess", 1.f);
        shader->set_uniform("shadow_map_depth", SHADOW_MAP_DEPTH_TEXTURE);
        shader->set_uniform("shadow_map_translucency",
                            SHADOW_MAP_TRANSLUCENCY_TEXTURE);
        shader->set_uniform("tex", LOCAL_TEXTURE0);

        glActiveTexture(GL_TEXTURE0 + LOCAL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
    }

    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, (width - 1) * (height - 1) * 2 * 3,
                   GL_UNSIGNED_INT, 0);
}

inline glm::vec3 terrain::to_surface(const glm::vec3& position)
{
    auto position_local = glm::inverse(get_model_matrix()) *
                          glm::vec4(position.x, 0., position.z, 1.);
    auto position_array =
        (glm::vec2(position_local.x, position_local.z) + glm::vec2(0.5)) *
        glm::vec2(height - 1, width - 1);
    position_local.y =
        elevation[int(position_array.x + 0.5)][int(position_array.y + 0.5)];
    return glm::vec3(get_model_matrix() * position_local);
}
