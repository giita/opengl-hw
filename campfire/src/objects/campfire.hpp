#pragma once

#include "../scene_object.hpp"

#include <memory>
#include <string>
#include <cstddef>
#include <functional>
#include <random>

#include <GL/glew.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "../util/opengl_shader.hpp"

#include "../util/obj_util.hpp"

#include "../util/stb_image.h"

#define EMITTERS_COUNT 2

struct particle {
    float time_born;
    float alive;
    glm::vec3 pos;
    glm::vec3 velocity;
};

struct emitter {
    std::vector<particle> particles;
    float period_ = 1;
    float life_;
    float last_spawn;
    bool spawned;
    std::function<std::pair<glm::vec3, glm::vec3>()> pos_f;
    float scale_;
    float onset_;

    GLuint vbo, vao;

    template <typename PosF>
    emitter(PosF pf, float period, int count, float scale, float onset)
        : particles(std::vector<particle>(count)),
          period_(period),
          life_(count * period),
          pos_f(pf),
          scale_(scale),
          onset_(onset)
    {
    }

    void init_gl()
    {
        glGenBuffers(1, &vbo);
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(particle) * particles.size(),
                     particles.data(), GL_DYNAMIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(particle),
                              (void*)offsetof(particle, pos));
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(particle),
                              (void*)offsetof(particle, alive));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(particle),
                              (void*)offsetof(particle, time_born));
        glBindVertexArray(0);
    }

    void draw(shader_t* shader)
    {
        shader->set_uniform("scale", scale_);
        shader->set_uniform("life", life_);
        shader->set_uniform("onset", onset_);
        glBindVertexArray(vao);
        glDrawArrays(GL_POINTS, 0, particles.size());
    }

    void update_particle(int i)
    {
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferSubData(GL_ARRAY_BUFFER, i * sizeof(particle), sizeof(particle),
                        &particles[i]);
    }

    void update(const draw_context& context)
    {
        for (int i = 0; i < particles.size(); ++i) {
            auto& p = particles[i];
            bool updated = false;
            if (context.time - p.time_born > life_) {
                p.alive = 0;
            }
            if (p.alive > 0.5) {
                p.pos += p.velocity * context.imgui_io->DeltaTime;
                updated = true;
            }
            if (updated) {
                update_particle(i);
            }
        }
        if (!spawned || (context.time - last_spawn) >= period_) {
            if (!spawned) {
                spawn(context);
                last_spawn = context.time;
                spawned = true;
                return;
            }
            while ((context.time - last_spawn) >= period_) {
                spawn(context);
                last_spawn += period_;
            }
        }
    }

    int find_particle()
    {
        int mini = 0;
        for (int i = 0; i < particles.size(); ++i) {
            if (particles[i].alive < 0.5) {
                return i;
            }
            if (particles[mini].time_born > particles[i].time_born) {
                mini = i;
            }
        }
        return mini;
    }

    void spawn(const draw_context& context)
    {
        int i = find_particle();
        auto& p = particles[i];
        p.alive = 1;
        p.time_born = context.time;
        auto param = pos_f();
        p.pos = param.first;
        p.velocity = param.second;
        update_particle(i);
    }
};

class campfire : public scene_object
{
   private:
    std::string fragment_shader;
    std::string vertex_shader;
    std::string geometry_shader;
    std::string fire_tex_file;
    std::string smoke_tex_file;
    std::mt19937 r;

    GLuint fire_tex, smoke_tex;

    std::array<emitter, EMITTERS_COUNT> emitters;

    std::unique_ptr<shader_t> shader;

    void update_particle(int i);
    void update(const draw_context& context);

   public:
    glm::vec3 position;
    float scale = 1;
    float rotate = 0;

    campfire(const std::string& vertex_shader,
             const std::string& fragment_shader,
             const std::string& geometry_shader,
             const std::string& fire_tex_file,
             const std::string& smoke_tex_file);

    void load_resources();
    void init_gl();
    glm::mat4 get_model_matrix();
    void draw(const draw_context& context);
};

inline campfire::campfire(const std::string& vertex_shader,
                          const std::string& fragment_shader,
                          const std::string& geometry_shader,
                          const std::string& fire_tex_file,
                          const std::string& smoke_tex_file)
    : emitters({emitter(
                    [this]() {
                        static std::uniform_real_distribution<> nd{-0.2, 0.2};
                        return std::make_pair(glm::vec3(nd(r), 0., nd(r)),
                                              glm::vec3(nd(r), 1., nd(r)));
                    },
                    0.02,
                    50,
                    1.f,
                    0.1f),
                emitter(
                    [this]() {
                        static std::uniform_real_distribution<> nd{-0.2, 0.2};
                        return std::make_pair(glm::vec3(nd(r), 0.7, nd(r)),
                                              glm::vec3(nd(r), 0.6, nd(r)));
                    },
                    0.15,
                    30,
                    2.f,
                    0.1f)})
{
    this->vertex_shader = vertex_shader;
    this->fragment_shader = fragment_shader;
    this->geometry_shader = geometry_shader;
    this->fire_tex_file = fire_tex_file;
    this->smoke_tex_file = smoke_tex_file;
    particles = true;
}

inline void campfire::load_resources()
{
    shader = std::make_unique<shader_t>(vertex_shader, fragment_shader,
                                        geometry_shader);

    for (int i = 0; i < 2; ++i) {
        GLuint& tex = i == 0 ? fire_tex : smoke_tex;
        glGenTextures(1, &tex);
        glBindTexture(GL_TEXTURE_2D, tex);

        int width, height, num_channels;

        unsigned char* data =
            stbi_load((i == 0 ? fire_tex_file : smoke_tex_file).c_str(), &width,
                      &height, &num_channels, 0);

        int mode = num_channels == 3 ? GL_RGB : GL_RGBA;

        glTexImage2D(GL_TEXTURE_2D, 0, mode, width, height, 0, mode,
                     GL_UNSIGNED_BYTE, data);
        stbi_image_free(data);
    }
}

inline void campfire::init_gl()
{
    glBindTexture(GL_TEXTURE_2D, fire_tex);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, smoke_tex);
    glGenerateMipmap(GL_TEXTURE_2D);

    for (auto& e : emitters) {
        e.init_gl();
    }
}

inline glm::mat4 campfire::get_model_matrix()
{
    return glm::translate(position) *
           glm::scale(glm::vec3(scale, scale, scale)) *
           glm::rotate(rotate, glm::vec3(0., 1., 0.));
}

inline void campfire::update(const draw_context& context)
{
    for (auto& e : emitters) {
        e.update(context);
    }
}

inline void campfire::draw(const draw_context& context)
{
    shader->use();
    glm::mat4 model = get_model_matrix();
    shader->set_uniform("model", model);
    shader->set_uniform("view", context.view);
    shader->set_uniform("projection", context.projection);
    shader->set_uniform("time", context.time);
    shader->set_uniform("tex", LOCAL_TEXTURE0);
    glActiveTexture(GL_TEXTURE0 + LOCAL_TEXTURE0);

    GLint blend_src, blend_dst;
    GLboolean depth_mask;
    glGetBooleanv(GL_DEPTH_WRITEMASK, &depth_mask);
    glGetIntegerv(GL_BLEND_SRC_ALPHA, &blend_src);
    glGetIntegerv(GL_BLEND_DST_ALPHA, &blend_dst);
    glDepthMask(GL_FALSE);

    if (context.drawing_particles_shadow) {
        shader->set_uniform("output_alpha", true);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glBindTexture(GL_TEXTURE_2D, smoke_tex);
        emitters[1].draw(shader.get());
        glBindTexture(GL_TEXTURE_2D, fire_tex);
        emitters[0].draw(shader.get());
    } else {
        shader->set_uniform("output_alpha", false);
        glBindTexture(GL_TEXTURE_2D, smoke_tex);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        emitters[1].draw(shader.get());

        glBlendFunc(GL_SRC_ALPHA, GL_ONE);
        glBindTexture(GL_TEXTURE_2D, fire_tex);
        emitters[0].draw(shader.get());
    }

    glDepthMask(depth_mask);
    glBlendFunc(blend_src, blend_dst);
}
