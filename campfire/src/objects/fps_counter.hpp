#pragma once

#include "../scene_object.hpp"

#include <imgui.h>

class fps_counter : public scene_object
{
   public:
    int x_, y_, h_;
    float saved_fr = -1;
    float last_time = -1;
    fps_counter(int x, int y, int h) : x_(x), y_(y), h_(h) {}
    void draw(const draw_context& context);
};

inline void fps_counter::draw(const draw_context& context)
{
    ImGui::Begin("Info", nullptr, ImGuiWindowFlags_AlwaysAutoResize);

    if (last_time == -1 || context.time - last_time > 0.5) {
        saved_fr = context.imgui_io->Framerate;
        last_time = context.time;
    }

    ImGui::Text("Draw time: %.3f ms", 1 / saved_fr * 1000);
    ImGui::Text("      FPS: %.3f", saved_fr);

    ImGui::End();
}
