#pragma once

#include "../scene_object.hpp"

#include <array>
#include <string>
#include <memory>

#include <GL/glew.h>
#include <GL/gl.h>

#include <glm/glm.hpp>

#include "../util/opengl_shader.hpp"

#include "../util/stb_image.h"

class skybox : public scene_object
{
   private:
    std::string fragment_shader;
    std::string vertex_shader;
    std::array<std::string, 6> textures;

    GLuint vao, vbo, ebo;
    GLuint texture;

    std::unique_ptr<shader_t> shader;

   public:
    float rotate = 0.;

    skybox(const std::string& fragment_shader,
           const std::string& vertex_shader,
           const std::array<std::string, 6>& textures);

    void load_resources();
    void init_gl();
    glm::mat4 get_model_matrix();
    void draw(const draw_context& context);
};

inline skybox::skybox(const std::string& vertex_shader,
                      const std::string& fragment_shader,
                      const std::array<std::string, 6>& textures)
{
    this->vertex_shader = vertex_shader;
    this->fragment_shader = fragment_shader;
    this->textures = textures;
}

inline void skybox::load_resources()
{
    shader = std::make_unique<shader_t>(vertex_shader, fragment_shader);

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

    int width, height, num_channels;
    unsigned char* data;
    stbi_set_flip_vertically_on_load(false);
    for (unsigned int i = 0; i < textures.size(); i++) {
        data =
            stbi_load(textures[i].c_str(), &width, &height, &num_channels, 0);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width,
                     height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        stbi_image_free(data);
    }
    stbi_set_flip_vertically_on_load(true);
}

inline void skybox::init_gl()
{
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    static GLfloat vertices[] = {
        1.0,  1.0,  1.0,   //
        1.0,  1.0,  -1.0,  //
        1.0,  -1.0, 1.0,   //
        1.0,  -1.0, -1.0,  //
        -1.0, 1.0,  1.0,   //
        -1.0, 1.0,  -1.0,  //
        -1.0, -1.0, 1.0,   //
        -1.0, -1.0, -1.0,  //
    };

    static GLuint indices[] = {
        5, 7, 3,  //
        3, 1, 5,  //

        6, 7, 5,  //
        5, 4, 6,  //

        3, 2, 0,  //
        0, 1, 3,  //

        6, 4, 0,  //
        0, 2, 6,  //

        5, 1, 0,  //
        0, 4, 5,  //

        7, 6, 3,  //
        3, 6, 2,  //
    };

    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);

    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices,
                 GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float),
                          (void*)0);
}

inline glm::mat4 skybox::get_model_matrix()
{
    return glm::rotate(rotate, glm::vec3(0., 1., 0.));
}

inline void skybox::draw(const draw_context& context)
{
    // ignore only_geometry since we will ignore skybox anyway
    shader->use();
    glm::mat4 view_rotate_only = context.projection *
                                 glm::mat4(glm::mat3(context.view)) *
                                 get_model_matrix();
    shader->set_uniform("transform", view_rotate_only);
    shader->set_uniform("cubemap", LOCAL_TEXTURE0);
    glActiveTexture(GL_TEXTURE0 + LOCAL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    glBindVertexArray(vao);

    GLboolean depth_test;
    glGetBooleanv(GL_DEPTH_TEST, &depth_test);
    glDisable(GL_DEPTH_TEST);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
    depth_test == GL_TRUE ? glEnable(GL_DEPTH_TEST) : glDisable(GL_DEPTH_TEST);
}
