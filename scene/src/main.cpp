#include "renderer.hpp"

int main(int, char**)
{
    renderer r;
    r.init();
    r.main_loop();
    return 0;
}
