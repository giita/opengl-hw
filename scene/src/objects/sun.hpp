#pragma once

#include "../scene_object.hpp"

#include <string>

#include <GL/glew.h>
#include <GL/gl.h>

#include "../util/stb_image.h"

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>

class sun : public scene_object
{
   public:
    glm::vec3 dir;
    glm::vec3 color;

    sun();
    void config_context(draw_context& context);
};

inline sun::sun()
{
    global_config = true;
    drawable = false;
}

void sun::config_context(draw_context& context)
{
    context.light_dir = dir;
    context.light_color = color;
    float near_plane = 0.0f, far_plane = 1000.0f;
    glm::mat4 light_projection =
        glm::ortho(-120.0f, 120.0f, -120.0f, 120.0f, near_plane, far_plane);
    glm::vec3 center =
        context.camera_position + context.camera_direction * glm::vec3(30.);
    glm::mat4 light_view =
        glm::lookAt(-context.light_dir * glm::vec3(500.) + center, center,
                    glm::vec3(0.0f, 1.0f, 0.0f));
    context.light_space = light_projection * light_view;
}
