#pragma once

#include "../scene_object.hpp"

#include <iostream>
#include <string>
#include <memory>

#include <GL/glew.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "../util/opengl_shader.hpp"

#include "../util/stb_image.h"

class water : public scene_object
{
   private:
    std::string fragment_shader;
    std::string vertex_shader;
    std::string normals_file;

    GLuint normals_tex;
    GLuint vao, vbo, ebo;
    GLuint fbo, texture, depth_buffer;

    std::unique_ptr<shader_t> shader;

   public:
    inline constexpr static unsigned int REFLECTION_WIDTH = 512,
                                         REFLECTION_HEIGHT = 512;
    inline constexpr static int END_COORD = 2000;

    float height = 0;

    water(const std::string& vertex_shader,
          const std::string& fragment_shader,
          const std::string& normals_file);

    glm::vec3 get_reflected_camera_pos(const draw_context& context);
    void draw_fbo(const draw_context& context);

    void load_resources();
    void init_gl();
    void draw(const draw_context& context);
};

inline water::water(const std::string& vertex_shader,
                    const std::string& fragment_shader,
                    const std::string& normals_file)
{
    this->vertex_shader = vertex_shader;
    this->fragment_shader = fragment_shader;
    this->normals_file = normals_file;
}

inline void water::load_resources()
{
    shader = std::make_unique<shader_t>(vertex_shader, fragment_shader);

    glGenTextures(1, &normals_tex);
    glBindTexture(GL_TEXTURE_2D, normals_tex);

    int width, height, num_channels;

    unsigned char* data =
        stbi_load(normals_file.c_str(), &width, &height, &num_channels, 0);

    int mode = num_channels == 3 ? GL_RGB : GL_RGBA;

    glTexImage2D(GL_TEXTURE_2D, 0, mode, width, height, 0, mode,
                 GL_UNSIGNED_BYTE, data);
    stbi_image_free(data);
}

inline void water::init_gl()
{
    // normals

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // framebuffer

    glGenFramebuffers(1, &fbo);

    glGenRenderbuffers(1, &depth_buffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depth_buffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, REFLECTION_WIDTH,
                          REFLECTION_HEIGHT);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                              GL_RENDERBUFFER, depth_buffer);

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    for (int i = 0; i < 6; ++i) {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA,
                     REFLECTION_WIDTH, REFLECTION_HEIGHT, 0, GL_RGBA,
                     GL_UNSIGNED_BYTE, nullptr);
    }

    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture, 0);

    glDrawBuffer(GL_COLOR_ATTACHMENT0);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        std::cerr << "Framebuffer incomplete\n";
        exit(1);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // water surface

    static GLfloat vertices[] = {
        1.0,  0.0, 1.0,   //
        -1.0, 0.0, 1.0,   //
        -1.0, 0.0, -1.0,  //
        1.0,  0.0, -1.0,  //
    };

    static GLuint indices[] = {
        2, 1, 0,  //
        2, 0, 3,  //
    };

    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);

    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices,
                 GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float),
                          (void*)0);
}

inline glm::mat4 oblique_clip(glm::mat4 proj, glm::vec4 plane)
{
    glm::vec4 q;

    q.x = (glm::sign(plane.x) + proj[2][0]) / proj[0][0];
    q.y = (glm::sign(plane.y) + proj[2][1]) / proj[1][1];
    q.z = -1.f;
    q.w = (1.f + proj[2][2]) / proj[3][2];

    glm::vec4 c = plane * (2.f / glm::dot(plane, q));

    proj[0][2] = c.x;
    proj[1][2] = c.y;
    proj[2][2] = c.z + 1.f;
    proj[3][2] = c.w;

    return proj;
}

inline glm::vec3 water::get_reflected_camera_pos(const draw_context& context)
{
    glm::vec3 camera_pos_refl = context.camera_position;
    camera_pos_refl.y = height - (context.camera_position.y - height);
    return camera_pos_refl;
}

inline void water::draw_fbo(const draw_context& context)
{
    static glm::vec3 dir[] = {glm::vec3(1., 0., 0.), glm::vec3(-1., 0., 0.),
                              glm::vec3(0., 1., 0.), glm::vec3(0., -1., 0.),
                              glm::vec3(0., 0., 1.), glm::vec3(0., 0., -1.)};
    static glm::vec3 up[] = {glm::vec3(0., -1., 0.), glm::vec3(0., -1., 0.),
                             glm::vec3(0., 0., 1.),  glm::vec3(0., 0., -1.),
                             glm::vec3(0., -1., 0.), glm::vec3(0., -1., 0.)};

    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glViewport(0, 0, REFLECTION_HEIGHT, REFLECTION_WIDTH);

    glm::vec3 camera_pos_refl = get_reflected_camera_pos(context);

    draw_context reflection_context = context;
    reflection_context.camera_position = camera_pos_refl;
    for (int i = 0; i < 6; ++i) {
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                                  GL_RENDERBUFFER, depth_buffer);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                               GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, texture, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        reflection_context.view =
            glm::lookAt(camera_pos_refl, camera_pos_refl + dir[i], up[i]);

        glm::mat4 proj =
            glm::perspective(glm::radians(90.f), 1.f, 0.1f, 1000.f);

        glm::vec4 plane =
            glm::transpose(glm::inverse(reflection_context.view)) *
            glm::vec4(0., 1., 0., -height);

        reflection_context.projection = oblique_clip(proj, plane);

        for (auto& d : *context.objects) {
            if (d->reflected) {
                d->draw(reflection_context);
            }
        }
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);
}

inline void water::draw(const draw_context& context)
{
    draw_fbo(context);

    shader->use();

    glm::mat4 model =
        glm::translate(glm::vec3(context.camera_position.x, height,
                                 context.camera_position.z)) *
        glm::scale(glm::vec3(END_COORD, 1, END_COORD));

    glActiveTexture(GL_TEXTURE0 + LOCAL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

    glActiveTexture(GL_TEXTURE0 + LOCAL_TEXTURE0 + 1);
    glBindTexture(GL_TEXTURE_2D, normals_tex);

    shader->set_uniform("model", model);
    shader->set_uniform("view", context.view);
    shader->set_uniform("projection", context.projection);
    shader->set_uniform("proj_space", context.projector_space);
    shader->set_uniform("camera_pos", context.camera_position);
    shader->set_uniform("proj_tex", PROJECTOR_TEXTURE);
    shader->set_uniform("reflection", LOCAL_TEXTURE0);
    shader->set_uniform("normals", LOCAL_TEXTURE0 + 1);
    shader->set_uniform("time", context.time);

    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}
