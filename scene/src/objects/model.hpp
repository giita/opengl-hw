#pragma once

#include "../scene_object.hpp"

#include <memory>
#include <string>

#include <GL/glew.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "../util/opengl_shader.hpp"

#include "../util/obj_util.hpp"

#include "../util/stb_image.h"

class model : public scene_object
{
   private:
    std::string fragment_shader;
    std::string vertex_shader;
    std::string obj_file;
    std::string tex_file;

    std::vector<tinyobj::material_t> materials;

    GLuint texture;

    std::vector<GLuint> vaos, vbos;
    std::vector<int> element_counts;

    std::unique_ptr<shader_t> shader;

   public:
    glm::vec3 position;
    float scale = 1;
    float rotate = 0;

    model(const std::string& vertex_shader,
          const std::string& fragment_shader,
          const std::string& obj_file,
          const std::string& tex_file);

    void load_resources();
    void init_gl();
    glm::mat4 get_model_matrix();
    void draw(const draw_context& context);
};

inline model::model(const std::string& vertex_shader,
                    const std::string& fragment_shader,
                    const std::string& obj_file,
                    const std::string& tex_file)
{
    this->vertex_shader = vertex_shader;
    this->fragment_shader = fragment_shader;
    this->obj_file = obj_file;
    this->tex_file = tex_file;
}

inline void model::load_resources()
{
    shader = std::make_unique<shader_t>(vertex_shader, fragment_shader);

    init_obj(obj_file, vaos, vbos, element_counts, materials);

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    int width, height, num_channels;

    unsigned char* data =
        stbi_load(tex_file.c_str(), &width, &height, &num_channels, 0);

    int mode = num_channels == 3 ? GL_RGB : GL_RGBA;

    glTexImage2D(GL_TEXTURE_2D, 0, mode, width, height, 0, mode,
                 GL_UNSIGNED_BYTE, data);
    stbi_image_free(data);
}

inline void model::init_gl()
{
    glBindTexture(GL_TEXTURE_2D, texture);
    glGenerateMipmap(GL_TEXTURE_2D);
}

inline glm::mat4 model::get_model_matrix()
{
    return glm::translate(position) *
           glm::scale(glm::vec3(scale, scale, scale)) *
           glm::rotate(rotate, glm::vec3(0., 1., 0.));
}

inline void model::draw(const draw_context& context)
{
    if (!context.only_geometry) {
        shader->use();
        glm::mat4 model = get_model_matrix();

        shader->set_uniform("model", model);
        shader->set_uniform("view", context.view);
        shader->set_uniform("projection", context.projection);
        shader->set_uniform("normal_model", get_normal_model_matrix());
        shader->set_uniform("proj_space", context.projector_space);
        shader->set_uniform("light_dir", context.light_dir);
        shader->set_uniform("light_color", context.light_color);
        shader->set_uniform("camera_pos", context.camera_position);
        shader->set_uniform("light_space", context.light_space);
        shader->set_uniform("shadow_map", SHADOW_MAP_TEXTURE);
        shader->set_uniform("proj_tex", PROJECTOR_TEXTURE);
        shader->set_uniform("tex", LOCAL_TEXTURE0);

        glActiveTexture(GL_TEXTURE0 + LOCAL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
    }

    for (int m = 0; m < (int)materials.size(); ++m) {
        if (!context.only_geometry) {
            shader->set_uniform_f3("material.ambient", materials[m].ambient);
            shader->set_uniform_f3("material.diffuse", materials[m].diffuse);
            shader->set_uniform_f3("material.specular", materials[m].specular);
            shader->set_uniform("material.shininess", materials[m].shininess);
        }

        glBindVertexArray(vaos[m]);
        glDrawArrays(GL_TRIANGLES, 0, element_counts[m]);
    }
}
