#pragma once

#include "../scene_object.hpp"

#include <string>

#include <GL/glew.h>
#include <GL/gl.h>

#include "../util/stb_image.h"

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>

class projector : public scene_object
{
   private:
    std::string tex_file;

    GLuint texture;

   public:
    glm::vec3 position, look_dir, look_up = glm::vec3(0., 1., 0.);
    int width, height;

    projector(const std::string& tex_file);

    void load_resources();
    void init_gl();
    void config_context(draw_context& context);
};

inline projector::projector(const std::string& tex_file)
{
    this->tex_file = tex_file;
    global_config = true;
    drawable = false;
}

inline void projector::load_resources()
{
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    int num_channels;

    unsigned char* data =
        stbi_load(tex_file.c_str(), &width, &height, &num_channels, 0);

    int mode = num_channels == 3 ? GL_RGB : GL_RGBA;

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, mode,
                 GL_UNSIGNED_BYTE, data);
    stbi_image_free(data);
}

inline void projector::init_gl()
{
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    float borderColor[] = {1.0f, 1.0f, 1.0f, 0.0f};
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
}

void projector::config_context(draw_context& context)
{
    glActiveTexture(GL_TEXTURE0 + PROJECTOR_TEXTURE);
    glBindTexture(GL_TEXTURE_2D, texture);

    glm::mat4 projector_projection = glm::perspective(
        glm::radians(20.f), (float)width / (float)height, 10.f, 1000.f);
    glm::mat4 projector_view =
        glm::lookAt(position, position + look_dir, look_up);
    context.projector_space = projector_projection * projector_view;
}
