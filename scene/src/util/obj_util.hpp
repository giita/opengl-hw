#pragma once

#include <string>
#include <iostream>

#include <GL/glew.h>
#include <GL/gl.h>

#include <tiny_obj_loader.h>

inline void init_obj(const std::string& path,
                     std::vector<GLuint>& vaos,
                     std::vector<GLuint>& vbos,
                     std::vector<int>& element_counts,
                     std::vector<tinyobj::material_t>& materials)
{
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::string err;

    std::string dir = path.substr(0, path.find_last_of("/\\") + 1);

    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err,
                                path.c_str(), dir.c_str());
    if (!err.empty()) {
        std::cerr << err << std::endl;
    }
    if (!ret) {
        exit(1);
    }

    int materials_count = materials.size();

    vaos.resize(materials_count);
    vbos.resize(materials_count);
    element_counts.resize(materials_count);

    std::vector<std::vector<float>> data;
    data.resize(materials_count);
    for (size_t s = 0; s < shapes.size(); s++) {
        size_t index_offset = 0;
        for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
            int m = shapes[s].mesh.material_ids[f];
            int fv = shapes[s].mesh.num_face_vertices[f];
            if (fv != 3) {
                std::cerr << "Some face is not a triangle" << std::endl;
                exit(1);
            }
            for (int v = 0; v < fv; v++) {
                tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
                for (int i = 0; i < 3; ++i) {
                    data[m].push_back(
                        attrib.vertices[3 * idx.vertex_index + i]);
                }
                for (int i = 0; i < 3; ++i) {
                    data[m].push_back(attrib.normals[3 * idx.normal_index + i]);
                }
                for (int i = 0; i < 2; ++i) {
                    data[m].push_back(
                        attrib.texcoords[2 * idx.texcoord_index + i]);
                }
            }
            index_offset += fv;
        }
    }

    glGenVertexArrays(materials_count, vaos.data());
    glGenBuffers(materials_count, vbos.data());

    for (int m = 0; m < materials_count; ++m) {
        element_counts[m] = data[m].size() / 8;

        glBindVertexArray(vaos[m]);

        glBindBuffer(GL_ARRAY_BUFFER, vbos[m]);
        glBufferData(GL_ARRAY_BUFFER, data[m].size() * sizeof(float),
                     data[m].data(), GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
                              (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
                              (void*)(3 * sizeof(float)));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
                              (void*)(6 * sizeof(float)));
    }
}
