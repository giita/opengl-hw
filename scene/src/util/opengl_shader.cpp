#include "opengl_shader.hpp"

#include <fstream>
#include <sstream>
#include <iostream>

#include <GL/glew.h>
#include <GL/gl.h>

#include <glm/glm.hpp>

namespace
{
std::string read_shader_code(const std::string& fname)
{
    std::stringstream file_stream;
    try {
        std::ifstream file(fname.c_str());
        file_stream << file.rdbuf();
    } catch (std::exception const& e) {
        std::cerr << "Error reading shader file: " << e.what() << std::endl;
    }
    return file_stream.str();
}
}  // namespace

shader_t::shader_t(const std::string& vertex_code_fname,
                   const std::string& fragment_code_fname)
{
    const auto vertex_code = read_shader_code(vertex_code_fname);
    const auto fragment_code = read_shader_code(fragment_code_fname);
    compile(vertex_code, fragment_code);
    link();
}

shader_t::~shader_t() {}

void shader_t::compile(const std::string& vertex_code,
                       const std::string& fragment_code)
{
    const char* vcode = vertex_code.c_str();
    vertex_id_ = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_id_, 1, &vcode, NULL);
    glCompileShader(vertex_id_);

    const char* fcode = fragment_code.c_str();
    fragment_id_ = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_id_, 1, &fcode, NULL);
    glCompileShader(fragment_id_);
    check_compile_error();
}

void shader_t::link()
{
    program_id_ = glCreateProgram();
    glAttachShader(program_id_, vertex_id_);
    glAttachShader(program_id_, fragment_id_);
    glLinkProgram(program_id_);
    check_linking_error();
    glDeleteShader(vertex_id_);
    glDeleteShader(fragment_id_);
}

void shader_t::use()
{
    glUseProgram(program_id_);
}

void shader_t::set_uniform(const std::string& name, const int val)
{
    glUniform1i(glGetUniformLocation(program_id_, name.c_str()), val);
}

void shader_t::set_uniform(const std::string& name, const bool val)
{
    glUniform1i(glGetUniformLocation(program_id_, name.c_str()), val);
}

void shader_t::set_uniform(const std::string& name, const float val)
{
    glUniform1f(glGetUniformLocation(program_id_, name.c_str()), val);
}

void shader_t::set_uniform(const std::string& name, const glm::vec2& val)
{
    glUniform2f(glGetUniformLocation(program_id_, name.c_str()), val[0],
                val[1]);
}

void shader_t::set_uniform(const std::string& name, const glm::vec3& val)
{
    glUniform3f(glGetUniformLocation(program_id_, name.c_str()), val[0], val[1],
                val[2]);
}

void shader_t::set_uniform(const std::string& name, const glm::vec4& val)
{
    glUniform4f(glGetUniformLocation(program_id_, name.c_str()), val[0], val[1],
                val[2], val[3]);
}

void shader_t::set_uniform(const std::string& name, const glm::mat3& val)
{
    glUniformMatrix3fv(glGetUniformLocation(program_id_, name.c_str()), 1,
                       GL_FALSE, &val[0][0]);
}

void shader_t::set_uniform(const std::string& name, const glm::mat4& val)
{
    glUniformMatrix4fv(glGetUniformLocation(program_id_, name.c_str()), 1,
                       GL_FALSE, &val[0][0]);
}

void shader_t::set_uniform_f3(const std::string& name, const float* const val)
{
    glUniform3fv(glGetUniformLocation(program_id_, name.c_str()), 1, val);
}

void shader_t::check_compile_error()
{
    int success;
    char info_log[1024];
    glGetShaderiv(vertex_id_, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertex_id_, 1024, NULL, info_log);
        std::cerr << "Error compiling vertex shader:\n"
                  << info_log << std::endl;
    }
    glGetShaderiv(fragment_id_, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(fragment_id_, 1024, NULL, info_log);
        std::cerr << "Error compiling fragment shader:\n"
                  << info_log << std::endl;
    }
}

void shader_t::check_linking_error()
{
    int success;
    char infoLog[1024];
    glGetProgramiv(program_id_, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(program_id_, 1024, NULL, infoLog);
        std::cerr << "Error linking shader program:\n" << infoLog << std::endl;
    }
}
