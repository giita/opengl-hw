#include <vector>
#include <memory>

#include <GL/glew.h>
#include <GL/gl.h>

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "scene_object.hpp"

#include "glfw_callbacks.hpp"

#include "renderer.hpp"

void renderer::glfw_init_window()
{
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, 4);

    window = glfwCreateWindow(1280, 720, "Scene", NULL, NULL);
    if (window == NULL) {
        exit(1);
    }

    glfwSetWindowUserPointer(window, this);
    glfwSetScrollCallback(window, glfw_scroll_callback);
    glfwSetCursorPosCallback(window, glfw_cursor_position_callback);
    glfwSetMouseButtonCallback(window, glfw_mouse_button_callback);
    glfwSetKeyCallback(window, glfw_key_callback);
    glfwSetFramebufferSizeCallback(window, glfw_framebuffer_size_callback);

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
}

float renderer::get_fov()
{
    return 1.0 * current_scroll / MAX_SCROLL * 150;
}

glm::vec3 renderer::get_camera_direction()
{
    glm::vec3 camera_direction = glm::vec3(0.f, 0.0f, 1.0f);
    float vert = (cursor_y / CURSOR_Y_ABS_CAP) * (glm::pi<float>() / 2) * 0.9;
    float hori = (cursor_x / CURSOR_X_WRAP) * glm::pi<float>() * 2;
    camera_direction = glm::rotateX(camera_direction, vert);
    camera_direction = glm::rotateY(camera_direction, -hori);
    return camera_direction;
}

glm::vec3 renderer::get_position_shift(const glm::vec3 camera_direction,
                                       float time_delta)
{
    glm::vec3 position_shift = glm::vec3(0., 0., 0.);
    glm::vec3 horiz_dir = camera_direction;
    horiz_dir[1] = 0.;
    horiz_dir = glm::normalize(horiz_dir);
    glm::vec3 shift_vector[] = {
        camera_direction,      glm::rotateY(horiz_dir, glm::pi<float>() / 2),
        -camera_direction,     glm::rotateY(horiz_dir, -glm::pi<float>() / 2),
        glm::vec3(0., 1., 0.), glm::vec3(0., -1., 0.),
    };

    static int keys[] = {GLFW_KEY_W, GLFW_KEY_A,     GLFW_KEY_S,
                         GLFW_KEY_D, GLFW_KEY_SPACE, GLFW_KEY_LEFT_SHIFT};

    float speed = running ? RUNNING_SPEED : MOVEMENT_SPEED;

    for (unsigned i = 0; i < (sizeof(keys) / sizeof(int)); ++i) {
        if (glfwGetKey(window, keys[i]) == GLFW_PRESS) {
            position_shift += shift_vector[i] * time_delta * speed;
        }
    }
    return position_shift;
}

void renderer::recalc_context(float time_delta)
{
    context.camera_direction = get_camera_direction();

    context.camera_position =
        context.camera_position +
        get_position_shift(context.camera_direction, time_delta);

    context.view =
        glm::lookAt(context.camera_position,
                    context.camera_position + context.camera_direction,
                    glm::vec3(0.0f, 1.0f, 0.0f));

    context.projection =
        glm::perspective(glm::radians(get_fov()),
                         (float)display_w / (float)display_h, 0.1f, 1000.0f);
}
