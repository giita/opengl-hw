#pragma once

#include <vector>
#include <memory>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "scene_object.hpp"
#include "util/opengl_shader.hpp"

#include "renderer_util.hpp"

#include "glfw_callbacks.hpp"

#include <GLFW/glfw3.h>

inline constexpr float MOVEMENT_SPEED = 20., RUNNING_SPEED = 50.;

inline constexpr int MIN_SCROLL = 0;
inline constexpr int MAX_SCROLL = 50;

inline constexpr int CURSOR_Y_ABS_CAP = 300;
inline constexpr int CURSOR_X_WRAP = 4 * CURSOR_Y_ABS_CAP;

class renderer
{
    int display_w, display_h;
    int current_scroll = 23;
    double last_cursor_x, last_cursor_y;
    double cursor_x, cursor_y;
    bool running = false;

    float get_fov();

    std::vector<std::shared_ptr<scene_object>> objects;
    draw_context context;
    GLFWwindow* window;

    void glfw_init_window();
    glm::vec3 get_camera_direction();
    glm::vec3 get_position_shift(const glm::vec3 camera_direction,
                                 float time_delta);
    void recalc_context(float time_delta);

    void init_shadow_map();
    void draw_shadow_map();

    void init_scene();
    void draw_scene();

   public:
    void init();
    void main_loop();

    friend void glfw_scroll_callback(GLFWwindow* window,
                                     double xoffset,
                                     double yoffset);
    friend void glfw_cursor_position_callback(GLFWwindow* window,
                                              double xpos,
                                              double ypos);
    friend void glfw_key_callback(GLFWwindow* window,
                                  int key,
                                  int scancode,
                                  int action,
                                  int mods);
    friend void glfw_framebuffer_size_callback(GLFWwindow* window,
                                               int width,
                                               int height);
};

inline void renderer::init()
{
    glfw_init();
    glfw_init_window();
    glew_init();
    init_gl();
    init_scene();

    context.objects = &objects;
}

inline void renderer::main_loop()
{
    glfwGetFramebufferSize(window, &display_w, &display_h);
    float last_time = glfwGetTime();

    while (!glfwWindowShouldClose(window)) {
        glfwPollEvents();

        context.time = glfwGetTime();
        context.time_delta = context.time - last_time;
        last_time = context.time;

        for (auto& d : objects) {
            d->animate(d.get(), context.time, d->animation_data);
        }

        for (auto& d : objects) {
            if (d->global_config) {
                d->config_context(context);
            }
        }

        recalc_context(context.time_delta);
        draw_scene();

        //        std::cout << "frame\n";
        //        std::cout << context.camera_position.x << ' '
        //                  << context.camera_position.y << ' '
        //                  << context.camera_position.z << std::endl;
        //        std::cout << context.camera_direction.x << ' '
        //                  << context.camera_direction.y << ' '
        //                  << context.camera_direction.z << std::endl;
        //        std::cout << cursor_x << ' ' << cursor_y << std::endl;

        context.frame_number += 1;

        glfwSwapBuffers(window);
    }

    glfwDestroyWindow(window);
    glfwTerminate();
}
