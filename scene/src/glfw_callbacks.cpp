#include "renderer.hpp"

#include <iostream>

#include <GLFW/glfw3.h>

void glfw_error_callback(int error, const char* description)
{
    std::cerr << "GLFW error " << error << ": " << description << std::endl;
}

void glfw_scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    (void)window;
    (void)xoffset;
    renderer* r = (renderer*)glfwGetWindowUserPointer(window);
    r->current_scroll += yoffset;
    if (r->current_scroll > MAX_SCROLL) {
        r->current_scroll = MAX_SCROLL;
    } else if (r->current_scroll < MIN_SCROLL) {
        r->current_scroll = MIN_SCROLL;
    }
}

void glfw_cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
    (void)window;
    renderer* r = (renderer*)glfwGetWindowUserPointer(window);

    static int last_mode = -1;
    static double last_cursor_x, last_cursor_y;
    int mode = glfwGetInputMode(window, GLFW_CURSOR);

    if (mode == GLFW_CURSOR_DISABLED && mode == last_mode) {
        r->cursor_x += xpos - last_cursor_x;
        r->cursor_y += ypos - last_cursor_y;

        if (r->cursor_y > CURSOR_Y_ABS_CAP) {
            r->cursor_y = CURSOR_Y_ABS_CAP;
        } else if (r->cursor_y < -CURSOR_Y_ABS_CAP) {
            r->cursor_y = -CURSOR_Y_ABS_CAP;
        }
    }

    last_cursor_x = xpos;
    last_cursor_y = ypos;

    last_mode = mode;
}

void glfw_mouse_button_callback(GLFWwindow* window,
                                int button,
                                int action,
                                int mods)
{
    (void)window;
    (void)mods;
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        if (glfwGetInputMode(window, GLFW_CURSOR) == GLFW_CURSOR_NORMAL) {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        } else {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        }
    }
}

void glfw_key_callback(GLFWwindow* window,
                       int key,
                       int scancode,
                       int action,
                       int mods)
{
    (void)scancode;
    (void)mods;
    renderer* r = (renderer*)glfwGetWindowUserPointer(window);
    if (key == GLFW_KEY_Q && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
    if (key == GLFW_KEY_C && action == GLFW_PRESS) {
        r->running = !r->running;
    }
}

void glfw_framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    (void)window;
    renderer* r = (renderer*)glfwGetWindowUserPointer(window);
    r->display_h = height;
    r->display_w = width;
}
