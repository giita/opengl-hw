#include <memory>

#include "objects/sun.hpp"
#include "objects/projector.hpp"
#include "objects/shadow_map.hpp"
#include "objects/model.hpp"
#include "objects/skybox.hpp"
#include "objects/terrain.hpp"
#include "objects/water.hpp"

#include "renderer.hpp"
#include "util/INIReader.h"

struct projector_animation_data {
    float speed;
    glm::vec3 direction;
};

struct boat_animation_data {
    float speed;
    float radius;
    glm::vec3 center;
};

void renderer::init_scene()
{
    INIReader reader("assets/config.ini");

    auto sun = std::make_shared<class sun>();
    sun->dir = glm::vec3(reader.GetFloat("sun_dir", "x", 0.),
                         reader.GetFloat("sun_dir", "y", 0.),
                         reader.GetFloat("sun_dir", "z", 0.));
    sun->color = glm::vec3(reader.GetFloat("sun_color", "r", 0.),
                           reader.GetFloat("sun_color", "g", 0.),
                           reader.GetFloat("sun_color", "b", 0.));
    objects.push_back(sun);

    auto projector = std::make_shared<class projector>(
        reader.Get("projector", "texture", ""));
    projector->animation_data = new projector_animation_data{
        .speed = reader.GetFloat("projector_anim", "speed", 1.),
        .direction = glm::vec3(reader.GetFloat("projector_anim_dir", "x", 0.),
                               reader.GetFloat("projector_anim_dir", "y", 0.),
                               reader.GetFloat("projector_anim_dir", "z", 0.))};
    projector->animate = [](scene_object* d, float time, void* data) {
        class projector* p = (class projector*)d;
        projector_animation_data* da = (projector_animation_data*)data;
        p->look_dir = glm::rotateY(da->direction, time * da->speed);
    };
    objects.push_back(projector);

    auto shadow = std::make_shared<shadow_map>("assets/shaders/shadow.vs",
                                               "assets/shaders/shadow.fs");
    objects.push_back(shadow);

    auto skybox = std::make_shared<class skybox>(
        "assets/shaders/skybox.vs", "assets/shaders/skybox.fs",
        std::array<std::string, 6>{
            reader.Get("skybox", "right", ""),
            reader.Get("skybox", "left", ""),
            reader.Get("skybox", "up", ""),
            reader.Get("skybox", "down", ""),
            reader.Get("skybox", "front", ""),
            reader.Get("skybox", "back", ""),
        });
    skybox->reflected = true;
    objects.push_back(skybox);

    auto boat = std::make_shared<model>(
        "assets/shaders/model.vs", "assets/shaders/model.fs",
        reader.Get("boat", "model", ""), reader.Get("boat", "texture", ""));
    boat->animation_data = new boat_animation_data{
        .speed = reader.GetFloat("boat_anim", "speed", 1.),
        .radius = reader.GetFloat("boat_anim", "radius", 10.),
        .center = glm::vec3(reader.GetFloat("boat_anim_center", "x", 0.),
                            reader.GetFloat("boat_anim_center", "y", 0.),
                            reader.GetFloat("boat_anim_center", "z", 0.))};
    boat->animate = [](scene_object* d, float time, void* data) {
        model* b = (model*)d;
        boat_animation_data* da = (boat_animation_data*)data;
        double progress = time * da->speed;
        b->position = glm::vec3(-da->radius * glm::cos(progress), 0,
                                da->radius * glm::sin(progress)) +
                      da->center;
        b->rotate = progress;
    };
    boat->casts_shadow = true;
    boat->reflected = true;
    objects.push_back(boat);

    auto ground = std::make_shared<terrain>(
        "assets/shaders/terrain.vs", "assets/shaders/terrain.fs",
        reader.Get("ground", "heightmap", ""),
        std::array<std::string, 2>{reader.Get("ground", "texture1", ""),
                                   reader.Get("ground", "texture2", "")});
    ground->position = glm::vec3(reader.GetFloat("ground_pos", "x", 0.),
                                 reader.GetFloat("ground_pos", "y", 0.),
                                 reader.GetFloat("ground_pos", "z", 0.));
    ground->h = reader.GetFloat("ground", "h", 3.);
    ground->horiz = reader.GetFloat("ground", "horiz", 100.);
    ground->texture_size = reader.GetFloat("ground", "texture_size", 20.);
    ground->casts_shadow = true;
    ground->reflected = true;
    objects.push_back(ground);

    auto lighthouse = std::make_shared<model>(
        "assets/shaders/model.vs", "assets/shaders/model.fs",
        reader.Get("lighthouse", "model", ""),
        reader.Get("lighthouse", "texture", ""));
    lighthouse->scale = reader.GetFloat("lighthouse", "scale", 1.);
    lighthouse->rotate = reader.GetFloat("lighthouse", "rotate", 0.);
    lighthouse->casts_shadow = true;
    lighthouse->reflected = true;
    objects.push_back(lighthouse);

    auto water = std::make_shared<class water>(
        "assets/shaders/water.vs", "assets/shaders/water.fs",
        reader.Get("water", "normals", ""));
    water->height = reader.GetFloat("water", "height", 0.);
    objects.push_back(water);

    stbi_set_flip_vertically_on_load(true);
    for (auto& d : objects) {
        d->load_resources();
        d->init_gl();
    }

    lighthouse->position =
        ground->to_surface(
            glm::vec3(reader.GetFloat("lighthouse_pos", "x", 0.), 0.,
                      reader.GetFloat("lighthouse_pos", "z", 0.))) -
        glm::vec3(0., reader.GetFloat("lighthouse", "depth", 0.), 0.);
    projector->position =
        lighthouse->position +
        glm::vec3(0., reader.GetFloat("projector", "elevation", 0.), 0.);

    context.camera_position = glm::vec3(reader.GetFloat("camera_pos", "x", 0.),
                                        reader.GetFloat("camera_pos", "y", 0.),
                                        reader.GetFloat("camera_pos", "z", 0.));
    cursor_x = reader.GetFloat("cursor", "x", 0.);
    cursor_y = reader.GetFloat("cursor", "y", 0.);
}

void renderer::draw_scene()
{
    glViewport(0, 0, display_w, display_h);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    for (auto& d : objects) {
        d->draw(context);
    }
}
