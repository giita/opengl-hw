Display a scene.

Build using `build.sh`. Run using `scene` while in the `build/bin` directory.

### Demo

![](demo.png)

### Controls
* Move using `WASD`.
* Shift to move vertically down, space to move up.
* Click left mouse button inside window to grab cursor, now you can move your mouse to look around. Click again to release cursor.
* Press `C` to increase speed. Press `C` again to decrease speed to initial value.
* Press `Q` to quit.
