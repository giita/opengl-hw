#version 330 core
out vec3 out_color;

in vec3 frag_pos;
in vec4 frag_pos_proj_space;

uniform sampler2D proj_tex;
uniform sampler2D normals;
uniform samplerCube reflection;
uniform vec3 camera_pos;
uniform float height;
uniform float time;

vec4 get_proj()
{
    if (frag_pos_proj_space.z < 0.) {
        return vec4(vec3(1.), 0.);
    }
    vec2 proj_coords_tex = frag_pos_proj_space.xy / frag_pos_proj_space.w;
    proj_coords_tex = proj_coords_tex * 0.5 + 0.5;
    return texture(proj_tex, proj_coords_tex).rgba;
}

vec3 get_normal(vec2 norm_coord)
{
    return (texture(normals, norm_coord).rbg - vec3(0.5)) * 2;
}

vec3 get_reflection()
{
    vec2 norm_coord1 = frag_pos.xz / 70 + vec2(1., -1.) * time / 9;
    vec2 norm_coord2 = -frag_pos.xz / 50 + vec2(1., -1.) * time / 12;
    vec3 normal = normalize(get_normal(norm_coord1) + get_normal(norm_coord2));
    return texture(reflection, reflect(frag_pos - camera_pos, normal)).rgb;
}

void main()
{
    vec4 proj = get_proj();
    vec3 color = mix(get_reflection(), vec3(0., 0., 0.3), 0.5);
    color = mix(color, proj.rgb, proj.a / 2);
    out_color = color;
}
