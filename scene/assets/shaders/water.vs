#version 330 core

layout (location = 0) in vec3 in_pos;

out vec3 frag_pos;
out vec4 frag_pos_proj_space;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 proj_space;

void main()
{
    vec4 world_position = model * vec4(in_pos, 1.0);
    frag_pos = vec3(world_position);
    frag_pos_proj_space = proj_space * world_position;
    gl_Position = projection * view * world_position;
}
