#version 330 core

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

uniform sampler2D tex;
uniform sampler2D shadow_map;
uniform sampler2D proj_tex;
uniform Material material;
uniform vec3 light_color;
uniform vec3 light_dir;
uniform vec3 camera_pos;

in vec2 tex_coords;
in vec3 normal_l;
in vec3 frag_pos;
in vec4 frag_pos_light_space;
in vec4 frag_pos_proj_space;

out vec3 out_color;

vec3 normal;

float get_shadow()
{
    vec3 proj_coords = frag_pos_light_space.xyz / frag_pos_light_space.w;
    proj_coords = proj_coords * 0.5 + 0.5;
    float closest_depth = texture(shadow_map, proj_coords.xy).r;
    float current_depth = proj_coords.z;
    if(current_depth > 1.) {
        return 0.;
    }
    float bias = 0.005;
    float shadow = current_depth - bias  > closest_depth  ? 1. : 0.;
    return shadow;
}

vec4 get_proj()
{
    if (frag_pos_proj_space.z < 0.) {
        return vec4(vec3(1.), 0.);
    }
    vec2 proj_coords_tex = frag_pos_proj_space.xy / frag_pos_proj_space.w;
    proj_coords_tex = proj_coords_tex * 0.5 + 0.5;
    return texture(proj_tex, proj_coords_tex).rgba;
}

void main()
{
    normal = normalize(normal_l);

    // ambient
    vec3 ambient = light_color * material.ambient * 0.1;

    // diffuse
    float diff = max(dot(normal, -light_dir), 0.0);
    vec3 diffuse = light_color * (diff * material.diffuse);

    // specular
    vec3 view_dir = normalize(camera_pos - frag_pos);
    vec3 reflect_dir = reflect(light_dir, normal);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0), material.shininess);
    vec3 specular = light_color * (spec * material.specular);

    vec4 proj = get_proj();
    proj.a /= 2;
    out_color = mix((ambient + (1.0 - get_shadow()) * (diffuse + specular)) * texture(tex, tex_coords).rgb,
                proj.rgb, proj.a);
}
